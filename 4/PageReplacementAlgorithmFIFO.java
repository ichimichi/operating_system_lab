import java.util.Scanner;

public class PageReplacementAlgorithmFIFO {
    private static int[] frames;
    private static int[] entries;
    private static int pointer = 0;

    public static void main(String[] args) {
        int frameSize;
        int entrySize;
        int pageFaultOcurrence = 0;

        Scanner in = new Scanner(System.in);

        System.out.print("\nNumber of Frames : ");
        frameSize = in.nextInt();

        System.out.print("\nNumber of Entries : ");
        entrySize = in.nextInt();

        frames = new int[frameSize];
        entries = new int[entrySize];

        for (int i = 0; i < frameSize; i++) {
            frames[i] = -999;
        }

        System.out.println("Enter Sequence : \n");
        for (int i = 0; i < entrySize; i++) {
            System.out.print((i+1)+" : ");
            entries[i] = in.nextInt();
        }
        System.out.println("");


        for (int i = 0; i < entrySize; i++) {
            int request = entries[i];
            System.out.print("Checking "+request);
            if (pageFault(request)) {
                System.out.print(" - Page Fault occured");
                pageFaultOcurrence++;
                frames[pointer] = request;
                pointer = (pointer + 1) % frameSize;
            }
            displayMemory();
        }

        System.out.print("\n\nNumber of Page Faults : ");
        System.out.println(pageFaultOcurrence);
    }

    public static boolean pageFault(int x) {
        for (int i : frames) {
            if (i == x) {
                return false;
            }
        }
        return true;
    }

    public static void displayMemory() {
        System.out.print("\n[ ");
        for (int i : frames) {
            System.out.print(i+" ");
        }
        System.out.println("]\n");
    }
}
