import java.util.Scanner;

public class PageReplacementAlgorithmLRU {
    private static int[][] frames;
    private static int[] entries;
    private static int pointer = 0;
    private static final int value = 0;
    private static final int count = 1;

    public static void main(String[] args) {
        int frameSize;
        int entrySize;
        int pageFaultOcurrence = 0;

        Scanner in = new Scanner(System.in);

        System.out.print("\nNumber of Frames : ");
        frameSize = in.nextInt();

        System.out.print("\nNumber of Entries : ");
        entrySize = in.nextInt();

        frames = new int[2][frameSize];
        entries = new int[entrySize];

        for (int i = 0; i < frameSize; i++) {
            frames[value][i] = -999;
            frames[count][i] = 0;

        }

        System.out.println("Enter Sequence : \n");
        for (int i = 0; i < entrySize; i++) {
            System.out.print((i + 1) + " : ");
            entries[i] = in.nextInt();
        }
        System.out.println("");

        for (int i = 0; i < entrySize; i++) {
            int request = entries[i];
            System.out.print("Checking " + request);
            if (pageFault(request)) {
                System.out.print(" - Page Fault occured");
                pageFaultOcurrence++;
                if (isFree()) {
                    pointer = getFreeIndex();
                    frames[value][pointer] = request;
                    frames[count][pointer] = 0;

                } else {
                    pointer = getLeastRecentlyUsed();
                    frames[value][pointer] = request;
                    frames[count][pointer] = 0;
                }

                // pointer = (pointer + 1) % frameSize;
            }
            else{
                resetCount(request);
            }
            increment();
            displayMemory();
            displayCount();
        }

        System.out.print("\n\nNumber of Page Faults : ");
        System.out.println(pageFaultOcurrence);
    }

    public static boolean pageFault(int x) {
        for (int i : frames[value]) {
            if (i == x) {
                return false;
            }
        }
        return true;
    }

    public static void resetCount(int x) {
        for (int i = 0 ; i < frames[value].length;i++) {
            if (frames[value][i] == x) {
                frames[count][i] = 0;
            }
        }
    }

    public static boolean isFree() {
        for (int i : frames[value]) {
            if (i == -999) {
                return true;
            }
        }
        return false;
    }

    public static int getFreeIndex() {
        for (int i = 0 ; i < frames[value].length;i++) {
            if (frames[value][i] == -999) {
                return i;
            }
        }
        return 0;
    }

    public static void increment() {
        for (int i = 0; i < frames[value].length; i++) {
            if (frames[value][i] != -999) {
                frames[count][i]++;
            }
        }
    }

    public static int getLeastRecentlyUsed() {

        int lru = 0;
        for (int i = 1; i < frames[value].length; i++) {
            if (frames[count][i] > frames[count][lru]) {
                lru = i;
            }
        }

        return lru;
    }

    public static void displayMemory() {
        System.out.print("\nFrames : [ ");
        for (int i : frames[value]) {
            System.out.print(i + " ");
        }
        System.out.println("]");
    }

    public static void displayCount() {
        System.out.print("Count  : [ ");
        for (int i : frames[count]) {
            System.out.print(i + " ");
        }
        System.out.println("]\n");
    }
}
