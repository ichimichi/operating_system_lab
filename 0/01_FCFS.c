#include <stdio.h>
#define MAX 30

int getBurstTime(int processes[], int burstTime[])
{
    int n, i;
    printf("Enter number of processes( max 30) :");
    scanf("%d", &n);

    printf("\nEnter Process Burst Time\n");
    for (i = 0; i < n; i++)
    {
        processes[i] = i + 1;
        printf("P[%d]:", i + 1);
        scanf("%d", &burstTime[i]);
    }

    return n;
}

void findWaitingTime(int n, int burstTime[], int waitingTime[])
{
    waitingTime[0] = 0;
    for (int i = 1; i <= n; i++)
    {
        waitingTime[i] = burstTime[i - 1] + waitingTime[i - 1];
    }
}

void printGanttChart(int n, int processes[], int burstTime[], int waitingTime[])
{
    int i, j;
    printf("\n\nGantt Chart\n");
    for (i = 0; i < n; i++)
    {
        printf("%-2d", processes[i]);
        for (j = 0; j < burstTime[i]; j++)
        {
            printf("-");
        }
    }

    printf("\n");
    for (i = 0; i <= n; i++)
    {
        printf("%-2d", waitingTime[i]);
        for (j = 0; j < burstTime[i]; j++)
        {
            printf(" ");
        }
    }
    printf("\n");
}

double findAvgTime(int n, int burstTime[], int waitingTime[])
{
    int i, totalWaitingTime = 0;
    for (i = 0; i < n; i++)
    {
        totalWaitingTime += waitingTime[i];
    }

    return totalWaitingTime / n;
}

int main()
{
    int processes[MAX];
    int burstTime[MAX] = {0};
    int waitingTime[MAX];

    int n = getBurstTime(processes, burstTime);

    findWaitingTime(n, burstTime, waitingTime);
    printf("\nAverage waiting time : %.2lf ms", findAvgTime(n, burstTime, waitingTime));
    printGanttChart(n, processes, burstTime, waitingTime);
    return 0;
}