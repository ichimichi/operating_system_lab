// Non-Preemptive Priority Scheduling
#include <stdio.h>

typedef struct
{
	int pid;
	int at;
	int bt;
	int priority;
	int ct;
	int tat;
	int wt;
} Process;

int getProcess(Process process[]);
double getAvgTAT(Process process[], int n);
double getAvgWT(Process process[], int n);
void displayExecution(Process process[], int n);
void nonPreemptivePriority(Process process[], int n);

int main()
{
	// // Solved Example Input
	// Process process[] = {
	// 	{1, 0, 4, 4},
	// 	{2, 1, 5, 5},
	// 	{3, 2, 1, 7},
	// 	{4, 3, 2, 2},
	// 	{5, 4, 3, 1},
	// 	{6, 5, 6, 6}};
	// int n = sizeof(process) / sizeof(process[0]);

	// User Input from keyboard 
	Process process[30] = {0};
	int n = getProcess(process);

	nonPreemptivePriority(process, n);
	displayExecution(process, n);

	return 0;
}

int getProcess(Process process[])
{
	int n, i;
	printf("Enter number of processes( max 30) : ");
	scanf("%d", &n);

	printf("\nEnter Arrival Time followed by Burst Time for the corresponding process\n");
	for (i = 0; i < n; i++)
	{
		process[i].pid = i + 1;
		printf("P[%d]:\t", process[i].pid);
		scanf("%d %d", &process[i].at, &process[i].bt);
	}

	return n;
}

void nonPreemptivePriority(Process process[], int n)
{

	int complete = 0, t = 0;
	int priority = 0;
	int found = 0;

	while (complete != n)
	{
		Process prioritizedProcess = {0, 0, 999, 0};

		for (int j = 0; j < n; j++)
		{
			if (process[j].at <= t &&
				process[j].priority >= prioritizedProcess.priority &&
				process[j].bt > 0 &&
				process[j].ct == 0)
			{
				if (process[j].priority == prioritizedProcess.priority)
				{
					if (process[j].at < prioritizedProcess.at)
					{
						prioritizedProcess = process[j];
						priority = j;
						found = 1;
					}
				}
				else
				{
					prioritizedProcess = process[j];
					priority = j;
					found = 1;
				}
			}
		}

		if (found == 0)
		{
			printf("%-3c", '-');
			t++;
		}
		else
		{
			printf("P%d-", process[priority].pid);
			for (int k = 1; k < process[priority].bt; k++)
			{
				printf("---");
			}

			found = 0;

			process[priority].ct = t + process[priority].bt;

			process[priority].tat = process[priority].ct - process[priority].at;

			process[priority].wt = process[priority].tat - process[priority].bt;

			complete++;
			t += process[priority].bt;
		}
	}
	printf("\n");
	for (int k = 0; k <= process[priority].ct; k++)
	{
		printf("%-3d", k);
	}
	printf("\n\n");
}

double getAvgTAT(Process process[], int n)
{
	int tat = 0;
	for (int i = 0; i < n; i++)
		tat += process[i].tat;

	return (double)tat / n;
}

double getAvgWT(Process process[], int n)
{
	int wt = 0;
	for (int i = 0; i < n; i++)
		wt += process[i].wt;

	return (double)wt / n;
}

void displayExecution(Process process[], int n)
{

	printf("Process No.\tArrival Time\tBurst time\tPriority\tCompletion Time\tTurn around time\tWaiting time\n");

	for (int i = 0; i < n; i++)
	{
		printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t\t%d\n", process[i].pid, process[i].at, process[i].bt, process[i].priority, process[i].ct, process[i].tat, process[i].wt);
	}

	printf("\nAverage waiting time = %.2lf ms", getAvgWT(process, n));
	printf("\nAverage turn around time = %.2lf ms\n", getAvgTAT(process, n));
}