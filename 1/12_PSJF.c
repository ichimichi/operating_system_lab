// Preemptive Shortest Job First Scheduling (or Shortest Remaining Job First)
#include <stdio.h>

typedef struct
{
	int pid;
	int at;
	int bt;
	int ct;
	int tat;
	int wt;
} Process;

int getProcess(Process process[]);
double getAvgTAT(Process process[], int n);
double getAvgWT(Process process[], int n);
void displayExecution(Process process[], int n);
void preemptiveSJF(Process process[], int n);

int main()
{
	// // Solved Example Input
	// Process process[] = {
	// 	{1, 3, 4},
	// 	{2, 4, 2},
	// 	{3, 5, 1},
	// 	{4, 2, 6},
	// 	{5, 1, 8},
	// 	{6, 2, 4}};
	// int n = sizeof(process) / sizeof(process[0]);

	// User Input from keyboard 
	Process process[30] = {0};
	int n = getProcess(process);

	preemptiveSJF(process, n);
	displayExecution(process, n);
	return 0;
}

int getProcess(Process process[])
{
    int n, i;
    printf("Enter number of processes( max 30) : ");
    scanf("%d", &n);

    printf("\nEnter Arrival Time followed by Burst Time for the corresponding process\n");
    for (i = 0; i < n; i++)
    {
        process[i].pid = i + 1;
        printf("P[%d]:\t", process[i].pid);
        scanf("%d %d", &process[i].at, &process[i].bt);
    }

    return n;
}

void preemptiveSJF(Process process[], int n)
{
	Process copy[n];
	for (int i = 0; i < n; i++)
		copy[i] = process[i];

	int complete = 0, t = 0;
	int shortest = 0, finish_time;
	int found = 0;

	Process shortestProcess = {0, 0, 999};

	while (complete != n)
	{

		for (int j = 0; j < n; j++)
		{
			if (process[j].at <= t &&
				copy[j].bt <= shortestProcess.bt &&
				copy[j].bt > 0)
			{
				if (copy[j].bt == shortestProcess.bt)
				{
					if (copy[j].at < shortestProcess.at)
					{
						shortestProcess = copy[j];
						shortest = j;
						found = 1;
					}
				}
				else
				{
					shortestProcess = copy[j];
					shortest = j;
					found = 1;
				}
			}
		}

		if (found == 0)
		{
			printf("%-3c", '-');
			t++;
		}
		else
		{
			printf("P%-2d", process[shortest].pid);

			copy[shortest].bt--;
			shortestProcess.bt = copy[shortest].bt;

			if (shortestProcess.bt == 0)
			{
				shortestProcess.bt = 999;
			}

			if (copy[shortest].bt == 0)
			{
				
				found = 0;

				process[shortest].ct = t + 1;

				process[shortest].tat = process[shortest].ct - process[shortest].at;

				process[shortest].wt = process[shortest].tat - process[shortest].bt;

				complete++;
			}
			t++;
		}
	}
	printf("\n");
	for (int k = 0; k <= process[shortest].ct; k++)
	{
		printf("%-3d", k);
	}
	printf("\n\n");
}

void displayExecution(Process process[], int n)
{

	printf("Process No.\tArrival Time\tBurst time\tCompletion Time\tTurn around time\tWaiting time\n");

	for (int i = 0; i < n; i++)
	{
		printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t\t%d\n", process[i].pid, process[i].at, process[i].bt, process[i].ct, process[i].tat, process[i].wt);
	}

	printf("\nAverage waiting time = %.2lf ms", getAvgWT(process, n));
	printf("\nAverage turn around time = %.2lf ms\n", getAvgTAT(process, n));
}

double getAvgTAT(Process process[], int n)
{
	int tat = 0;
	for (int i = 0; i < n; i++)
		tat += process[i].tat;

	return (double)tat / n;
}

double getAvgWT(Process process[], int n)
{
	int wt = 0;
	for (int i = 0; i < n; i++)
		wt += process[i].wt;

	return (double)wt / n;
}
