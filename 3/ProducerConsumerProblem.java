import java.util.Random;
import java.util.Scanner;

public class ProducerConsumerProblem{
    static final int N = 5;
    
    static Monitor mon = new Monitor();
    public static void main(String args[]) {

        Scanner in = new Scanner(System.in);
        int n;

        System.out.print("\n1.Produce\n2.Consume\n0.Exit");
        while (true) {

            System.out.print("\n> ");

            n = in.nextInt();
            switch (n) {
                case 1:
                    producer p = new producer();
                    p.start();
                    break;
                case 2:
                    consumer c = new consumer();
                    c.start();
                    break;
                case 0:
                    return;
            }
                        try {
                Thread.sleep(800);

            } catch (InterruptedException ex) {
                // TODO: handle exception
            }
            mon.displayBuffer();

        }
    }

    static class producer extends Thread {
        public void run() { 
            int item;
            item = produce_item();
            System.out.println("producer waiting to produce " + item);
            mon.insert(item);
            System.out.println("producer produced " + item);

        }

        private int produce_item() {
            Random random = new Random();
            return random.nextInt(200);
        } // actually produce
    }

    static class consumer extends Thread {
        public void run() { 
            int item;
            System.out.println("consumer waiting to consume....");
            item = mon.remove();
            System.out.println("consumer consumed " + item);

            consume_item(item);

        }

        private void consume_item(int item) {
        } // actually consume
    }

    static class Monitor { 
        private int buffer[] = new int[N];
        private int count = 0;

        public synchronized void insert(int val) {
            if (count == N)
                go_to_sleep(); // if the buffer is full, go to sleep
            
            buffer[count] = val; // insert an item into the buffer
            count = count + 1;

            if (count == 1)
                notify();   // if consumer was sleeping, wake it up
        }

        public synchronized int remove() {
            int val;
            if (count == 0)
                go_to_sleep(); // if the buffer is empty, go to sleep
            
            val = buffer[count-1];
            count = count - 1;

            if (count == N - 1)
                notify(); // if producer was sleeping, wake it up
            
            return val;
        }

        private void go_to_sleep() {
            try {
                wait();
            } catch (InterruptedException ex) {
                // TODO: handle exception
            }
        }

        public synchronized void displayBuffer() {
            int i;
            System.out.print("\nBuffer : ");
            for (i = 0; i < count; i++) {
                System.out.print(" " + buffer[i]);
            }
        }
    }
}