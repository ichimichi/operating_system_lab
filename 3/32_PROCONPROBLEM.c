#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define BUFFER_SIZE 5

pthread_mutex_t mutex;
pthread_t tid;
sem_t empty;
sem_t full;
int buffer[BUFFER_SIZE];
int counter;

void initializeData();
int insert_item(int item);
int remove_item();
void *producer(void *param);
void *consumer(void *param);
void displayBuffer();

int main()
{
    int n;

    initializeData();

    printf("\n1.Produce\n2.Consume\n0.Exit");
    while (1)
    {
        sleep(1);
        displayBuffer();
        printf("\n\n> ");
        scanf("%d", &n);
        switch (n)
        {
        case 1:
            pthread_create(&tid, NULL, producer, NULL);
            break;
        case 2:
            pthread_create(&tid, NULL, consumer, NULL);
            break;
        case 0:
            exit(0);
            break;
        }
    }

    pthread_join(tid, NULL);

    return 0;
}

void initializeData()
{
    pthread_mutex_init(&mutex, NULL);
    sem_init(&full, 0, 0);
    sem_init(&empty, 0, BUFFER_SIZE);
    counter = 0;
}

int insert_item(int item)
{
    if (counter < BUFFER_SIZE)
    {
        buffer[counter] = item;
        counter++;
        return item;
    }
    else
    {
        return -1;
    }
}

int remove_item()
{
    int item;
    if (counter > 0)
    {
        item = buffer[counter - 1];
        counter--;
        return item;
    }
    else
    {
        return -1;
    }
}

void *producer(void *param)
{
    int item;
    item = 20 + rand() % (200 - 20 + 1);

    printf("\nproducer waiting to produce %d....", item);
    sem_wait(&empty);
    pthread_mutex_lock(&mutex);
    printf("\nproducer produced %d", insert_item(item));
    pthread_mutex_unlock(&mutex);
    sem_post(&full);
    pthread_exit(NULL);
}

void *consumer(void *param)
{
    printf("\nconsumer waiting to consume....");
    sem_wait(&full);
    pthread_mutex_lock(&mutex);

    printf("\nconsumer consumed %d", remove_item());

    pthread_mutex_unlock(&mutex);
    sem_post(&empty);
    pthread_exit(NULL);
}

void displayBuffer()
{
    int i;
    printf("\nBuffer : ");
    for (i = 0; i < counter; i++)
    {
        printf("%d ", buffer[i]);
    }
}