#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define N 5
#define LEFT (n + N - 1) % N
#define RIGHT (n + 1) % N
#define THINKING 0
#define HUNGRY 1
#define EATING 2


int state[N];
int philosopher[N] = {0, 1, 2, 3, 4};

pthread_t thread_id[N];
pthread_mutex_t mutex;
sem_t S[N];

void initializeData();
void test(int n);
void take_fork(int n);
void put_fork(int n);
void *philosopherf(void *n);

int main()
{
	int i;

	initializeData();

	for (i = 0; i < N; i++)
	{
		pthread_create(&thread_id[i], NULL, philosopherf, &philosopher[i]);
	}

	for (i = 0; i < N; i++)
		pthread_join(thread_id[i], NULL);
}

void initializeData()
{
	int i;
    pthread_mutex_init(&mutex, NULL);

	for (i = 0; i < N; i++){
		sem_init(&S[i], 0, 0);
	}
}

void *philosopherf(void *n)
{

	while (1)
	{
		int i = *((int*)n);
		printf("philosopher %d : Thinking\n", i + 1);
		sleep(1);
		take_fork(i);
		printf("philosopher %d : Eating\n", i + 1);
		sleep(0);
		put_fork(i);
	}

	pthread_exit(NULL);
}

void test(int n)
{
	if (state[n] == HUNGRY && state[LEFT] != EATING && state[RIGHT] != EATING)
	{
		state[n] = EATING;
		// sleep(2);
		sem_post(&S[n]);
	}
}

void take_fork(int n)
{

	pthread_mutex_lock(&mutex);
	state[n] = HUNGRY;
	printf("philosopher %d : Hungry\n", n + 1);
	test(n);
	pthread_mutex_unlock(&mutex);
	sem_wait(&S[n]);
	sleep(1);
}

void put_fork(int n)
{
	pthread_mutex_lock(&mutex);
	state[n] = THINKING;
	printf("philosopher %d : Thinking\n", n + 1);
	test(LEFT);
	test(RIGHT);
	pthread_mutex_unlock(&mutex);
}



