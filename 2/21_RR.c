// Round Robin Scheduling
#include <stdio.h>

typedef struct
{
	int pid;
	int at;
	int bt;
	int ct;
	int tat;
	int wt;
} Process;

typedef struct
{
	int front, rear;
	Process p[300];
} Queue;

void queueEnqueue(Queue *q, Process p)
{
	if (q->rear == 300)
	{
		printf("\nQueue is full\n");
		return;
	}
	else
	{
		q->p[q->rear] = p;
		q->rear++;
	}
	return;
}

void queueDequeue(Queue *q)
{
	int i;
	if (q->front == q->rear)
	{
		printf("\nQueue is empty\n");
		return;
	}
	else
	{
		for ( i = 0; i < q->rear - 1; i++)
		{
			q->p[i] = q->p[i + 1];
		}
		q->rear--;
	}
	return;
}

Process *queueFront(Queue *q)
{
	if (q->front == q->rear)
	{
		printf("\nQueue is Empty\n");
		return NULL;
	}
	return q->p;
}

int getProcess(Process process[]);
double getAvgTAT(Process process[], int n);
double getAvgWT(Process process[], int n);
void displayExecution(Process process[], int n);
void RoundRobin(Process process[], int n, int quantum);

int main()
{
	// //Solved Example Input
	// Process process[] = {
	// 	{1, 0, 4},
	// 	{2, 1, 5},
	// 	{3, 2, 2},
	// 	{4, 3, 1},
	// 	{5, 4, 6},
	// 	{6, 5, 3}};
	// int n = sizeof(process) / sizeof(process[0]);

	// User Input from keyboard
	Process process[30] = {0};
	int n = getProcess(process);

	int timeQuantum;
	printf("\nEnter Time Quantum : ");
	scanf("%d", &timeQuantum);

	RoundRobin(process, n, timeQuantum);
	displayExecution(process, n);
	return 0;
}

int getProcess(Process process[])
{
	int n, i;
	printf("Enter number of processes( max 30) : ");
	scanf("%d", &n);

	printf("\nEnter Arrival Time followed by Burst Time for the corresponding process\n");
	for (i = 0; i < n; i++)
	{
		process[i].pid = i + 1;
		printf("P[%d]:\t", process[i].pid);
		scanf("%d %d", &process[i].at, &process[i].bt);
	}

	return n;
}

void RoundRobin(Process process[], int n, int quantum)
{
	int i,j;
	Process copy[n];
	for ( i = 0; i < n; i++)
		copy[i] = process[i];

	int complete = 0, t = 0;
	int found = 0;

	Queue readyQ;
	readyQ.front = readyQ.rear = 0;
	int cycles = 0;

	while (complete != n)
	{

		for ( j = 0; j < n; j++)
		{
			if (copy[j].at <= t &&
				// copy[j].bt <= shortestProcess.bt &&
				copy[j].bt > 0)
			{
				copy[j].at = 9999;
				queueEnqueue(&readyQ, copy[j]);
				found = 1;
			}
		}

		if (found == 0)
		{
			printf("%-3c", '-');
			t++;
		}
		else
		{
			if (cycles == quantum)
			{
				cycles = 0;
				Process temp = *queueFront(&readyQ);
				queueDequeue(&readyQ);
				queueEnqueue(&readyQ, temp);
			}

			printf("P%-2d", queueFront(&readyQ)->pid);

			queueFront(&readyQ)->bt--;
			cycles++;

			if (queueFront(&readyQ)->bt == 0)
			{
				Process temp = *queueFront(&readyQ);
				queueDequeue(&readyQ);
				// found = 0;

				process[temp.pid - 1].ct = t + 1;

				process[temp.pid - 1].tat = process[temp.pid - 1].ct - process[temp.pid - 1].at;

				process[temp.pid - 1].wt = process[temp.pid - 1].tat - process[temp.pid - 1].bt;

				complete++;
				cycles = 0;
			}

			t++;
		}
	}
	printf("\n");
	for ( i = 0; i <= t; i++)
	{
		printf("%-3d", i);
	}
	printf("\n\n");
}

void displayExecution(Process process[], int n)
{
	int i;
	printf("Process No.\tArrival Time\tBurst time\tCompletion Time\tTurn around time\tWaiting time\n");

	for ( i = 0; i < n; i++)
	{
		printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t\t%d\n", process[i].pid, process[i].at, process[i].bt, process[i].ct, process[i].tat, process[i].wt);
	}

	printf("\nAverage waiting time = %.2lf ms", getAvgWT(process, n));
	printf("\nAverage turn around time = %.2lf ms\n", getAvgTAT(process, n));
}

double getAvgTAT(Process process[], int n)
{
	int i,tat = 0;
	for ( i = 0; i < n; i++)
		tat += process[i].tat;

	return (double)tat / n;
}

double getAvgWT(Process process[], int n)
{
	int i,wt = 0;
	for ( i = 0; i < n; i++)
		wt += process[i].wt;

	return (double)wt / n;
}
